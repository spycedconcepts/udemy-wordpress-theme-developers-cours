This file only exists to include the uploads folder in the repo

The uploads folder is required to support the docker-compose.yml build
and to enable a shared uploads volume.

Dev's are free to include whatever uploads they want as part of their
local dev build, but should not upload media resources to the git repo