# udemy-wordpress-theme-developers-cours
Udemy Wordpress Theme Developers Course


## Local Hosting

The repo is configured to use IP 127.0.0.3:8080 as a default.  This can be changed in the docker-compose.yml file at root.
This repo is intended for use as a Docker build.  All that is required is an installation of docker CE/Desktop
and for the dev to execute the following from the command line

```
docker-compose up --build -d
```

After the first build, unless there are any changes to the docker-compose.yml, the `--build` flag can be replaced.

##  Shared Volumes

The repo has been built with 4 folders within html/wp-content as shared volumes.
This means that changes can be made via your chose editor or via file copy, and these
will automatically update within the docker containers.

The folders are

- wp-content/mu-plugins
- wp-content/plugins
- wp-content/themes
- wp-content/uploads

These are the only folders where developed "modules" should be placed in a production 
environment, and so this has been replicated here.

All other wordpress folders only exist in the docker containers.

##  Persistence and Refreshing

The shared volumes allow file persistence for dev and theme projects, as well as 
local media files.  The database is not persisted, and this is by intention.

The database will exist as long as the containers created during the 
docker-compose build or docker-compose up --built instructions are not overwritten
or deleted.

To start from a fresh wordpress install 

